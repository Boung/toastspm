//
//  ToastView.swift
//  Trackhere
//
//  Created by Boung on 8/3/24.
//

import UIKit

final class ToastView: UIView {
  
  private var messageLabel: BaseLabel = {
    let label = BaseLabel(with: .init(top: 16, left: 16, bottom: 16, right: 16))
    label.textAlignment = .center
    label.textColor = .white
    label.backgroundColor = .defaultToastBackgroundColor
    label.font = .boldSystemFont(ofSize: 15)
    label.numberOfLines = 0
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    prepareLayouts()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func prepareLayouts() {
    messageLabel.layoutView {
      addSubview($0)
      $0.fill(toSafeArea: false)
    }
    
    messageLabel.setAllCornerRadius(constant: 8)
  }
  
  func setToast(_ message: String?) {
    messageLabel.text = message
  }
  
  func configToast(handler: ((BaseLabel) -> Void)?) {
    handler?(messageLabel)
  }
}
