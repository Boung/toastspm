//
//  Toast.swift
//  Trackhere
//
//  Created by Boung on 8/3/24.
//

import UIKit

public class Toast {
  public static let shared = Toast()
  
  /// Toast Fading Animation Duration
  public var toastAnimationDuration: TimeInterval = 0.3
  
  /// Toast Display Duration
  public var toastDuration: TimeInterval = 1.5
  
  /// Toast Background Color
  public var toastBackgorundColor: UIColor = .defaultToastBackgroundColor
  
  /// Toast Text Color
  public var toastTextColor: UIColor = .white
  
  private lazy var toastView = ToastView()
  private var timer: Timer?
  
  private init() { 
    
  }
  
  /// Show Toast
  /// - Parameters:
  ///   - controller: ViewController that will show Toast on
  ///   - message: The content message string that will show on Toast
  public func show(_ controller: UIViewController?, message: String?) {
    toastView.setToast(message)
    prepareToast(controller: controller)
  }
  
  private func prepareToast(controller: UIViewController?) {
    timer?.invalidate()
    
    guard let view = controller?.view else { return }
    removeToastView()
    
    toastView.configToast { [weak self] in
      $0.backgroundColor = self?.toastBackgorundColor
      $0.textColor = self?.toastTextColor
    }
    
    UIView.animate(withDuration: toastAnimationDuration, delay: 0, options: .transitionCrossDissolve) { [weak self] in
      guard let self = self else { return }
      
      self.toastView.alpha = 1
      self.toastView.layoutView {
        view.addSubview($0)
        $0.centerX()
        $0.top(constraint: view.layoutMarginsGuide.topAnchor, constant: 16, priority: .greaterThanOrEqaul)
        $0.leading(constraint: view.leadingAnchor, constant: 16, priority: .greaterThanOrEqaul)
        $0.bottom(constraint: view.layoutMarginsGuide.bottomAnchor, constant: 16)
      }
    } completion: { [weak self] _ in
      self?.timer = Timer.scheduledTimer(withTimeInterval: self?.toastDuration ?? 1, repeats: false) { [weak self] _ in
        guard let self = self else { return }
        self.removeToastView()
      }
    }

    toastView.layoutIfNeeded()
  }
  
  private func removeToastView() {
    toastView.alpha = 0
    toastView.removeFromSuperview()
  }
}
