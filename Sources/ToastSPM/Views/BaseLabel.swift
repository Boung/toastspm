//
//  BaseLabel.swift
//  Trackhere
//
//  Created by Boung on 10/12/23.
//

import UIKit

class BaseLabel: UILabel {
  
  var topInset: CGFloat
  var bottomInset: CGFloat
  var leftInset: CGFloat
  var rightInset: CGFloat
  
  init(with inset: UIEdgeInsets = .zero) {
    self.topInset = inset.top
    self.bottomInset = inset.bottom
    self.leftInset = inset.left
    self.rightInset = inset.right
    super.init(frame: .zero)
    
    configDefaultProperties()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func drawText(in rect: CGRect) {
    let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    super.drawText(in: rect.inset(by: insets))
  }
  
  override var intrinsicContentSize: CGSize {
    get {
      var contentSize = super.intrinsicContentSize
      contentSize.height += topInset + bottomInset
      contentSize.width += leftInset + rightInset
      return contentSize
    }
  }
  
  private func configDefaultProperties() {
    textAlignment = .left
    textColor = .black
    font = .systemFont(ofSize: 15)
    numberOfLines = 1
  }
}
