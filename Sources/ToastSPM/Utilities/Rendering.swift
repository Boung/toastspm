//
//  File.swift
//  
//
//  Created by Boung on 10/3/24.
//

import UIKit

extension UIView {
  func setAllCornerRadius(constant: CGFloat) {
    layoutIfNeeded()
    clipsToBounds = true
    layer.cornerRadius = constant
    layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
  }
}
