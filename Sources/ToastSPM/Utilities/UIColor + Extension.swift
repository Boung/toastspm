//
//  File.swift
//  
//
//  Created by Boung on 10/3/24.
//

import UIKit

extension UIColor {
  static let defaultToastBackgroundColor: UIColor = .black.withAlphaComponent(0.8)
}
