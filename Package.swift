// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ToastSPM",
    platforms: [
      .iOS(.v12),
    ],
    products: [
        .library(
            name: "ToastSPM",
            targets: ["ToastSPM"]),
    ],
    targets: [
        .target(
            name: "ToastSPM"),
    ]
)
